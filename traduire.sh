saxonb-xslt -s:"exemples/Christophe Parisse.xml" -xsl:"gabarits/traduction fr.xsl" -o:"exemples/Christophe Parisse traduit.xml" chemin_traduction="../traductions/tei_français.odd"
saxonb-xslt -s:"exemples/Christophe Parisse traduit.xml" -xsl:"gabarits/traduction fr.xsl" -o:"exemples/Christophe Parisse retraduit.xml" chemin_traduction="../traductions/tei_français.odd" sens="intraduction"
diff "exemples/Christophe Parisse.xml" "exemples/Christophe Parisse retraduit.xml"

saxonb-xslt -s:"exemples/Michel Jacobson.xml" -xsl:"gabarits/traduction fr.xsl" -o:"exemples/Michel Jacobson traduit.xml" chemin_traduction="../traductions/tei_français.odd"
saxonb-xslt -s:"exemples/Michel Jacobson traduit.xml" -xsl:"gabarits/traduction fr.xsl" -o:"exemples/Michel Jacobson retraduit.xml" chemin_traduction="../traductions/tei_français.odd" sens="intraduction"
diff "exemples/Michel Jacobson.xml" "exemples/Michel Jacobson retraduit.xml"