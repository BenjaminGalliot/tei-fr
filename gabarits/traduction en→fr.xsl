<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.tei-c.org/ns/1.0" xmlns:xalan="http://xml.apache.org/xslt" xmlns:tei="http://www.tei-c.org/ns/1.0" version="2.0" xml:lang="fr">
<xsl:output method="xml" doctype-public="-//W3C//DTD HTML 5 Transitional//FR" encoding="utf-8" indent="yes" xalan:indent-amount="4"/>

<xsl:param name="chemin_traduction"/>
<xsl:param name="sens" select="'extraduction'"/>

<xsl:variable name="traductions" select="document($chemin_traduction)"/>

<xsl:template match="*">
  <xsl:variable name="nom_élément">
    <xsl:call-template name="traduire_élément"/>
  </xsl:variable>
  <xsl:element name="{$nom_élément}">
    <xsl:for-each select="@*">
      <xsl:variable name="nom_attribut">
        <xsl:call-template name="traduire_attribut"/>
      </xsl:variable>
      <xsl:attribute name="{$nom_attribut}">
        <xsl:value-of select="."/>
      </xsl:attribute>
    </xsl:for-each>
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

<xsl:template name="traduire_élément">
  <xsl:variable name="élément" select="replace(name(.), '[ʻʼ]', '')"/>
  <xsl:variable name="traduction">
    <xsl:choose>
      <xsl:when test="$sens = 'extraduction'">
        <xsl:value-of select="$traductions//tei:elementSpec[@ident = $élément]/tei:altIdent/text()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$traductions//tei:elementSpec[tei:altIdent/text() = $élément]/@ident"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:if test="count($traduction) != 1">
    <xsl:message>Nombre de traductions d’élément trouvées incorrect (<xsl:value-of select="count($traduction)"/>) pour <xsl:value-of select="$élément"/> : <xsl:value-of select="$traduction"/></xsl:message>
  </xsl:if>
  <xsl:value-of select="if ($traduction != '') then $traduction else if ($sens = 'extraduction') then concat('ʻ', $élément, 'ʼ') else $élément"/>
</xsl:template>

<xsl:template name="traduire_attribut">
  <xsl:variable name="attribut" select="replace(name(.), '[ʻʼ]', '')"/>
  <xsl:variable name="traduction">
    <xsl:choose>
      <xsl:when test="$sens = 'extraduction'">
        <xsl:value-of select="distinct-values($traductions//tei:attDef[@ident = $attribut]/tei:altIdent/text())"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="distinct-values($traductions//tei:attDef[tei:altIdent/text() = $attribut]/@ident)"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:if test="count($traduction) != 1">
    <xsl:message>Nombre de traductions d’attribut trouvées incorrect (<xsl:value-of select="count($traduction)"/>) pour <xsl:value-of select="$attribut"/> : <xsl:value-of select="$traduction"/></xsl:message>
  </xsl:if>
  <xsl:value-of select="if ($traduction != '') then $traduction else if ($sens = 'extraduction') then concat('ʻ', $attribut, 'ʼ') else $attribut"/>
</xsl:template>

</xsl:stylesheet>
